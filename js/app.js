import books from "./data.js";

const divRoot = document.getElementById("root");
const list = document.createElement("ul");

makeMarkup(books, list, divRoot);

function makeMarkup(arr, ul, div) {
  arr.forEach(obj => {
    const arrOfObjKeys = Object.keys(obj);
    const arrOfKeys = ["author", "name", "price"];

    try {
      for (let key of arrOfKeys) {
        if (!arrOfObjKeys.includes(key)) {
          throw new Error(`Такої властивості як ${key} немає в об'єкті`);
        }
      }
      const markup = `<li class="list__item">Autor: ${obj.author}; name: ${obj.name}; price: ${obj.price}</li>`;
      list.classList.add("list");
      ul.insertAdjacentHTML("beforeend", markup);
    } catch (error) {
      console.log(error);
    }
  });
  div.insertAdjacentElement("afterbegin", ul);
}
